A Blender addon - puts a handful of animation-related quick-reference tools in the 3D viewport UI panel.  
-Keyframe type, and operators to change type from the viewport  
-Keyframe handles, operators to change current frame or all  
-Operator to move all keys, or only current + before / after, by a user-specified number of frames   
-Operator to change interpolation of current or all keyframes

